/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import datos.*;

/**
 *
 * @author Dell
 */
public class LogicaLab3 {

    DatosLab3 dl = new DatosLab3();

    public String imprimirData4(int cedula) {
        dl.traer4(cedula);
        String retorno = "";
        for (String datos : dl.pistaListaArray4()) {
            retorno += datos + "\n";
        }
        return retorno;
    }

    public LinkedList<String> imprimirDataEquipos() {
        dl.traerEquipos();
        return dl.equiposListaArray();
    }

    public LinkedList<String> imprimirDataFortaleza() {
        dl.traerFortaleza();
        return dl.fortalezaListaArray();
    }

    public LinkedList<String> imprimirDataPerfil() {
        dl.traerPerfil();
        return dl.perfilListaArray();
    }

    public void actualiza(int cedula, String estado, int equipo, int destreza) {
        boolean nuevo = false;
        if (estado.equalsIgnoreCase("Activo")) {
            nuevo = true;
        } else if (estado.equalsIgnoreCase("Inactivo")) {
            nuevo = false;
        }
        dl.actualiza(cedula, nuevo, equipo, destreza);
    }

    public boolean saveImage(int cedula, String ruta) {
        try {
            dl.guardarfoto(cedula, ruta);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public ImageIcon imagenConsulta(int cedula) throws IOException {
        dl.traerImagen(cedula);
        ByteArrayInputStream bis = new ByteArrayInputStream(dl.datosBytesFoto());
        Iterator readers = ImageIO.getImageReadersByFormatName("jpeg");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis;
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        ImageIcon newicon = new ImageIcon(reader.read(0, param));
        return newicon;
    }

}
