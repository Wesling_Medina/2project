/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.*;
import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

/**
 *
 * @author estudiante
 */
public class LogicaLab2 {

    DatosLab2 dl = new DatosLab2();

    public String imprimirData() {
        dl.traer();
        String retorno = "";
        for (String datos : dl.pistaListaArray()) {
            retorno += datos + "\n";
        }
        return retorno;
    }

    public String imprimirData2(String uno, String dos) {
        dl.traer2(uno, dos);
        String retorno = "";
        for (String datos : dl.pistaListaArray2()) {
            retorno += datos + "\n";
        }
        System.out.println(retorno);
        return retorno;
    }

    public String imprimirData3(String equipo, int uno, int dos) {
        dl.traer3(equipo, uno, dos);
        String retorno = "";
        for (String datos : dl.pistaListaArray3()) {
            retorno += datos + "\n";
        }
        return retorno;
    }

}
