/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.*;

/**
 *
 * @author Dell
 */
public class LogicaLab4 {

    private int cedula;
    DatosLab4 dl = new DatosLab4();

    public LogicaLab4() {
    }

    public LogicaLab4(int cedula) {
        this.cedula = cedula;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public boolean eliminar(int cedula) {
        return dl.eliminarJugador(cedula);
    }
}
