/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Wesling Medina
 */
public class Conexion {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;



    public Connection Conexion() {
        if (connection != null) {
            return connection;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "12345";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                return connection;
            }
        } catch (Exception e) {
            System.out.println("Error de conexion");
        }
        return connection;
    }

}
