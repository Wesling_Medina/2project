/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 *
 * @author Dell
 */
public class DatosLab2 {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    Conexion con = new Conexion();

    private LinkedList<String> pistaLista;
    private LinkedList<String> pistaLista2;
    private LinkedList<String> pistaLista3;

    public void traer() {
        pistaLista = new LinkedList<>();
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select jugadores.cedula,jugadores.nombre,equipo.nombre_equipo,destrezas.perfil from jugadores join destrezas on (destrezas.fortaleza = 'velocidad' and jugadores.edad > 20 and jugadores.estado = true) and jugadores.destreza = destrezas.id_destreza join equipo on jugadores.equipo = equipo.id_equipo");

            while (rs.next()) {
                pistaLista.add(Integer.toString(rs.getInt("cedula")) + "," + rs.getString("nombre") + "," + rs.getString("nombre_equipo") + "," + rs.getString("perfil")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void traer2(String primero, String segundo) {
        pistaLista2 = new LinkedList<>();

        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select j.nombre,e.nombre_equipo,j.fecha_debut from jugadores j, equipo e where j.equipo = e.id_equipo And j.fecha_debut between '" + primero + "' and '" + segundo + "'group by j.nombre,e.nombre_equipo,j.fecha_debut");

            while (rs.next()) {
                pistaLista2.add(rs.getString("nombre") + "," + rs.getString("nombre_equipo") + "," + rs.getString("fecha_debut")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void traer3(String equipo, int uno, int dos) {
        pistaLista3 = new LinkedList<>();

        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select j.cedula, j.nombre, j.edad,j.fecha_debut from jugadores j, equipo e, destrezas d where(j.estado = true and e.nombre_equipo = '" + equipo + "' and d.fortaleza = 'cabeceo' and e.estado = true and j.fecha_debut > '1997-01-01' and j.edad between '" + uno + "' and '" + dos + "')  and(j.equipo = e.id_equipo) and(j.destreza = d.id_destreza)");

            while (rs.next()) {
                pistaLista3.add(Integer.toString(rs.getInt("cedula")) + "," + rs.getString("nombre") + "," + Integer.toString(rs.getInt("edad")) + "," + rs.getString("fecha_debut")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> pistaListaArray() {
        return pistaLista;
    }

    public LinkedList<String> pistaListaArray2() {
        return pistaLista2;
    }

    public LinkedList<String> pistaListaArray3() {
        return pistaLista3;
    }

}
