/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Dell
 */
public class DatosLab3 {

    Conexion con = new Conexion();
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    private LinkedList<String> equiposLista;
    private LinkedList<String> perfilLista;
    private LinkedList<String> fortalezaLista;
    private LinkedList<String> pistaLista4;
    private byte[] bytesImagen;

    public LinkedList<String> perfilListaArray() {
        return perfilLista;
    }

    public LinkedList<String> equiposListaArray() {
        return equiposLista;
    }

    public LinkedList<String> fortalezaListaArray() {
        return fortalezaLista;
    }

    public void traerEquipos() {
        equiposLista = new LinkedList<>();
        
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select nombre_equipo from equipo");

            while (rs.next()) {
                equiposLista.add(rs.getString("nombre_equipo")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void traerPerfil() {
        perfilLista = new LinkedList<>();
        
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select perfil from destrezas\n"
                    + "	group by perfil");

            while (rs.next()) {
                perfilLista.add(rs.getString("perfil")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public boolean guardarfoto(int cedula, String ruta) {
        FileInputStream fis = null;
        try {
            File file = new File(ruta);
            fis = new FileInputStream(file);

            PreparedStatement pstm = con.Conexion().prepareStatement("UPDATE jugadores SET foto = ?  WHERE cedula = " + cedula);
            pstm.setBinaryStream(1, fis, (int) file.length());
            pstm.execute();
            pstm.close();
            return true;
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return false;
    }

    public void traerImagen(int cedula) {
        
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select foto from jugadores where cedula = " + cedula);

            while (rs.next()) {
                bytesImagen = rs.getBytes("foto");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public boolean actualiza(int cedula, boolean estado, int equipo, int destreza) {
        
        try {
            s = con.Conexion().createStatement();
            int z = s.executeUpdate("UPDATE jugadores SET estado = '" + estado + "', equipo = '" + equipo + "', destreza = '" + destreza + "' WHERE cedula = '" + cedula + "'");
            if (z == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void traerFortaleza() {
        fortalezaLista = new LinkedList<>();
        
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select fortaleza from destrezas\n"
                    + "	group by fortaleza");

            while (rs.next()) {
                fortalezaLista.add(rs.getString("fortaleza")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public byte[] datosBytesFoto() {
        return bytesImagen;
    }

    public void traer4(int cedula) {
        pistaLista4 = new LinkedList<>();
        try {
            s = con.Conexion().createStatement();
            rs = s.executeQuery("select j.nombre,j.edad,e.nombre_equipo,d.perfil,d.fortaleza,j.fecha_debut,j.estado from jugadores j, equipo e, destrezas d\n"
                    + "	where (j.cedula = '" + cedula + "' and j.equipo = e.id_equipo and j.destreza = d.id_destreza) and e.estado = true");

            while (rs.next()) {
                pistaLista4.add(rs.getString("nombre") + "," + Integer.toString(rs.getInt("edad")) + "," + rs.getString("nombre_equipo") + "," + rs.getString("perfil") + "," + rs.getString("fortaleza") + "," + rs.getString("fecha_debut") + "," + String.valueOf(rs.getBoolean("estado"))
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> pistaListaArray4() {
        return pistaLista4;
    }

}
